#define PISDC		class
#define PISDCONST	const
#define PISDCCAST	const_cast
#define PISDCE		constexpr
#define PISDDCAST	dynamic_cast
#define PISDELSE	else
#define PISDE		enum
#define PISDNO		false
#define PISDFLOOP	for
#define PISDIF		if
#define PISDIL		inline
#define PISDNS		namespace
#define PISDNP		nullptr
#define PISDRCAST	reinterpret_cast
#define PISDRET		return
#define PISDSCAST	static_cast
#define PISDT		template
#define PISDYES		true
#define PISDTN		typename
#define PISDUSING	using
#define PISDWLOOP	while
#define PISDADD		+
#define PISDAND		&&
#define PISDASSIGN	=
#define PISDBAND	&
#define PISDBOR		|
#define PISDBSL		<<
#define PISDBSR		>>
#define PISDCB		)
#define PISDCCB		}
#define PISDCOMMA	,
#define PISDCQB		]
#define PISDDEC		--
#define PISDDIV		/
#define PISDEQUALS	==
#define PISDINC		++
#define PISDINHERIT	:
#define PISDINNS	::
#define PISDLESS	<
#define PISDLESSEQ	<=
#define PISDMEMBER	.
#define PISDMOD		%
#define PISDMORE	>
#define PISDMOREEQ	>=
#define PISDMUL		*
#define PISDNBIT	~
#define PISDNEQUALS	!=
#define PISDNOT		!
#define PISDOB		(
#define PISDOCB		{
#define PISDOQB		[
#define PISDOR		||
#define PISDPOINT	->
#define PISDQ		?
#define PISDSC		;
#define PISDSUB		-
#define PISDXOR		^

#include <iostream>
#include <string>

PISDNS pisdstd_n PISDASSIGN std PISDSC
PISDUSING pisdstr_t PISDASSIGN pisdstd_n PISDINNS string PISDSC
PISDUSING pisdwstr_t PISDASSIGN pisdstd_n PISDINNS wstring PISDSC
PISDUSING pisd_t PISDASSIGN void PISDSC
PISDUSING pisdb_t PISDASSIGN bool PISDSC
PISDUSING pisdc_t PISDASSIGN char PISDSC
PISDUSING pisdwc_t PISDASSIGN wchar_t PISDSC
PISDUSING pisdi_t PISDASSIGN int PISDSC
PISDUSING pisdui_t PISDASSIGN unsigned int PISDSC
PISDUSING pisds_t PISDASSIGN short PISDSC
PISDUSING pisdus_t PISDASSIGN unsigned short PISDSC
PISDUSING pisdl_t PISDASSIGN long PISDSC
PISDUSING pisdul_t PISDASSIGN unsigned long PISDSC
PISDUSING pisdf_t PISDASSIGN float PISDSC
PISDUSING pisdd_t PISDASSIGN double PISDSC
PISDUSING pisdld_t PISDASSIGN long double PISDSC
PISDUSING pisdnp_t PISDASSIGN nullptr_t PISDSC

PISDE PISDC EPisd PISDINHERIT pisdi_t PISDOCB
    pisd PISDASSIGN 0
PISDCCB PISDSC

PISDT PISDLESS PISDTN PISDMORE
PISDCE pisdb_t pisd PISDOB PISDCB PISDOCB
	PISDRET pisdstd_n PISDINNS cin PISDMEMBER good PISDOB PISDCB PISDSC
PISDCCB

PISDT PISDLESS PISDTN PISDCOMMA PISDTN PISDMORE
PISDCE pisdb_t pisd PISDOB pisdc_t c PISDCB PISDOCB
	PISDRET isalnum PISDOB c PISDCB PISDSC
PISDCCB

PISDT PISDLESS PISDTN PISDCOMMA PISDTN PISDCOMMA PISDTN PISDMORE
PISDCE pisdc_t pisd PISDOB PISDCB PISDOCB
	PISDRET PISDSCAST PISDLESS pisdc_t PISDMORE PISDOB pisdstd_n PISDINNS cin PISDMEMBER get PISDOB PISDCB PISDCB PISDSC
PISDCCB

PISDT PISDLESS PISDTN PISDCOMMA PISDTN PISDCOMMA PISDTN PISDCOMMA PISDTN PISDCOMMA PISDTN Pisd PISDMORE
PISDCE pisd_t pisd PISDOB PISDCONST Pisd PISDBAND pisd PISDCB PISDOCB
	pisdstd_n PISDINNS cout PISDBSL pisd PISDSC
PISDCCB
