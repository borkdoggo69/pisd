# pisd

## Description
Turn standard input into a set of `pisd`

## Why?
Why not?

## Examples
```bash
pisd | echo "Hello World!"
```

```bash
pisd < some_text_file.txt
```

```bash
pisd < english.txt > pisd.txt
```

## Images
![pisd](https://cdn.discordapp.com/attachments/282227039354748940/486204420942725131/unknown.png)